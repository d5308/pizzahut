package com.pizzahut.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orderhistory")
public class OrderHistory {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	
	private int orderHistoryId;
	
	private int userId;
	private int sizeId;
	private int toppingId;
	private int quantity;
	private double totalPrice;
	
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="payId")
	private Payments payment;
	

	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="deliveryId")
	private DeliveryStatus delivery ;
	
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="addressId")
	private Address addresses ;
	
	public OrderHistory() {
		// TODO Auto-generated constructor stub
	}

	public OrderHistory(int orderHistoryId, int userId, int sizeId, int toppingId, int quantity, double totalPrice,
			Payments payment, DeliveryStatus delivery, Address addresses) {
		super();
		this.orderHistoryId = orderHistoryId;
		this.userId = userId;
		this.sizeId = sizeId;
		this.toppingId = toppingId;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
		this.payment = payment;
		this.delivery = delivery;
		this.addresses = addresses;
	}

	public int getOrderHistoryId() {
		return orderHistoryId;
	}

	public void setOrderHistoryId(int orderHistoryId) {
		this.orderHistoryId = orderHistoryId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public int getToppingId() {
		return toppingId;
	}

	public void setToppingId(int toppingId) {
		this.toppingId = toppingId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Payments getPayment() {
		return payment;
	}

	public void setPayment(Payments payment) {
		this.payment = payment;
	}

	public DeliveryStatus getDelivery() {
		return delivery;
	}

	public void setDelivery(DeliveryStatus delivery) {
		this.delivery = delivery;
	}

	public Address getAddresses() {
		return addresses;
	}

	public void setAddresses(Address addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return String.format(
				"OrderHistory [orderHistoryId=%s, userId=%s, sizeId=%s, toppingId=%s, quantity=%s, totalPrice=%s, payment=%s, delivery=%s, addresses=%s]",
				orderHistoryId, userId, sizeId, toppingId, quantity, totalPrice, payment, delivery, addresses);
	}

	
}
