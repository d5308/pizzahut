package com.pizzahut.entities;



import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cart")
public class Cart {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	
	private int cartId;
	
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="userId")
	private Users user ;
	
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="sizeId")
	private ItemSize itemSize ;
	
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="toppingId")
	private Toppings toppings ;
	
	private int quantity;
	
	private double price;//(is.price+t.price)*quantity
	
	public Cart() {
		// TODO Auto-generated constructor stub
	}

	public Cart(int cartId, Users user, ItemSize itemSize, Toppings toppings, int quantity, double price) {
		super();
		this.cartId = cartId;
		this.user = user;
		this.itemSize = itemSize;
		this.toppings = toppings;
		this.quantity = quantity;
		this.price = price;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public ItemSize getItemSize() {
		return itemSize;
	}

	public void setItemSize(ItemSize itemSize) {
		this.itemSize = itemSize;
	}

	public Toppings getToppings() {
		return toppings;
	}

	public void setToppings(Toppings toppings) {
		this.toppings = toppings;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return String.format("Cart [cartId=%s, user=%s, itemSize=%s, toppings=%s, quantity=%s, price=%s]", cartId, user,
				itemSize, toppings, quantity, price);
	}

			
}
