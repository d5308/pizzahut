package com.pizzahut.entities;



import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="feedback")
public class Feedback {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int feedbackId;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="userId")
	private Users user;
	private String feedback;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date feedBackTime;
	
	public Feedback() {
		super();
	}

	public Feedback(int feedbackId, Users user, String feedback, Date feedBackTime) {
		super();
		this.feedbackId = feedbackId;
		this.user = user;
		this.feedback = feedback;
		this.feedBackTime = feedBackTime;
	}

	public int getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(int feedbackId) {
		this.feedbackId = feedbackId;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public Date getFeedBackTime() {
		return feedBackTime;
	}

	public void setFeedBackTime(Date feedBackTime) {
		this.feedBackTime = feedBackTime;
	}

	@Override
	public String toString() {
		return String.format("Feedback [feedbackId=%s, user=%s, feedback=%s, feedBackTime=%s]", feedbackId, user,
				feedback, feedBackTime);
	}

	
}
