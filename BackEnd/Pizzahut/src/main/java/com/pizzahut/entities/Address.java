package com.pizzahut.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="address")
public class Address {

	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Id
	private int addressId;
	@ManyToOne(cascade =CascadeType.MERGE)
	@JoinColumn(name="userId")
	private Users users;
	
	private String plotNo;
	private String streetName;
	private String city;
	private String district;
	private String soverignState;
	private String pinCode;
	
	public Address() {
		// TODO Auto-generated constructor stub
	}

	public Address(int addressId, Users users, String plotNo, String streetName, String city, String district,
			String soverignState, String pinCode) {
		super();
		this.addressId = addressId;
		this.users = users;
		this.plotNo = plotNo;
		this.streetName = streetName;
		this.city = city;
		this.district = district;
		this.soverignState = soverignState;
		this.pinCode = pinCode;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getSoverignState() {
		return soverignState;
	}

	public void setSoverignState(String soverignState) {
		this.soverignState = soverignState;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public String toString() {
		return String.format(
				"Address [addressId=%s, users=%s, plotNo=%s, streetName=%s, city=%s, district=%s, soverignState=%s, pinCode=%s]",
				addressId, users, plotNo, streetName, city, district, soverignState, pinCode);
	}

	
}
