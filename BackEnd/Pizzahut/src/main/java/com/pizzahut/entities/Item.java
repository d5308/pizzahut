package com.pizzahut.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {

	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Id
	private int itemId;
	
	private String type;
	
	private String itemName;
	
	private String description;
	
	
	public Item() {
		// TODO Auto-generated constructor stub
	}


	public Item(int itemId, String type, String itemName, String description) {
		super();
		this.itemId = itemId;
		this.type = type;
		this.itemName = itemName;
		this.description = description;
	}


	public int getItemId() {
		return itemId;
	}


	public void setItemId(int itemId) {
		this.itemId = itemId;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return String.format("Item [itemId=%s, type=%s, itemName=%s, description=%s]", itemId, type, itemName,
				description);
	}
	
}