package com.pizzahut.entities;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="toppingImages")
public class ToppingImages {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	
	private int toppingImgId;
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="toppingId")
	private Toppings toppings;
	@Lob
	private byte[] Data;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date topTime;
	
	public ToppingImages() {
		// TODO Auto-generated constructor stub
	}

	public ToppingImages(int toppingImgId, Toppings toppings, byte[] data, Date topTime) {
		super();
		this.toppingImgId = toppingImgId;
		this.toppings = toppings;
		Data = data;
		this.topTime = topTime;
	}

	public int getToppingImgId() {
		return toppingImgId;
	}

	public void setToppingImgId(int toppingImgId) {
		this.toppingImgId = toppingImgId;
	}

	public Toppings getToppings() {
		return toppings;
	}

	public void setToppings(Toppings toppings) {
		this.toppings = toppings;
	}

	public byte[] getData() {
		return Data;
	}

	public void setData(byte[] data) {
		Data = data;
	}

	public Date getTopTime() {
		return topTime;
	}

	public void setTopTime(Date topTime) {
		this.topTime = topTime;
	}

	@Override
	public String toString() {
		return String.format("ToppingImages [toppingImgId=%s, toppings=%s, Data=%s, topTime=%s]", toppingImgId,
				toppings, Arrays.toString(Data), topTime);
	}

	
}
