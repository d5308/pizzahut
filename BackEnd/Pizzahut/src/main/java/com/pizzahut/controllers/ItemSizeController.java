package com.pizzahut.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.entities.ItemSize;
import com.pizzahut.services.ItemSizeService;

@Controller
@RestController
@RequestMapping("/itemSize")
public class ItemSizeController {

	@Autowired
	private ItemSizeService itemSizeService;
	
	@GetMapping("/{itemid}/{size}")
	public ResponseEntity<?> fetchByItemSizeId(@PathVariable("itemid") int itemId, @PathVariable("size") String size) {
		ItemSize sizeOfPizza = itemSizeService.SizeOfPizza(size, itemId);
		if(sizeOfPizza != null) {
			return Response.success(sizeOfPizza);
		}else {
			return Response.error("This pizza is not available in this Size");
		}
	}
}
