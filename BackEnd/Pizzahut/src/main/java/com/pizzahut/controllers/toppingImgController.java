package com.pizzahut.controllers;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.ToppingImageDto;
import com.pizzahut.entities.ToppingImages;
import com.pizzahut.services.toppingImgService;

@Controller
@RestController
@RequestMapping("/toppingImages")
public class toppingImgController {

	@Autowired
	private toppingImgService imgService;
	@Autowired
	private DtoEntityConvertor convertor;
	
	@PostMapping("/add/{toppingId}")
	public ResponseEntity<?> addToppingImage(@PathVariable("toppingId") int toppingId,ToppingImageDto toppingImageDto)
	{
		System.out.println(toppingImageDto.getTopTime());
		Map<String, Object> result=imgService.addToppingImage(toppingId,toppingImageDto);
		return Response.success(result);
	}
	
	@GetMapping(value="/attachment/{toppingImgId}",produces = "image/png")
	public @ResponseBody byte[] showAttachment(@PathVariable("toppingImgId")int toppingImgId)
	{
		ToppingImages attachment=imgService.findByImageId(toppingImgId);
		
		if(attachment==null)
			return null;
		return attachment.getData();
	}
	
//	@GetMapping(value="/attachment",produces="image/png")
//	public @ResponseBody ResponseEntity<?> showAll()
//	{
//		List<ToppingImages> list=imgService.showAll();
//		System.out.println();
//		Stream<ToppingImageDto> result=list.stream().map(ar->
//		convertor.toToppingImg(ar));
//		return Response.success(result);
//	}
}
