package com.pizzahut.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.AddressDto;
import com.pizzahut.entities.Address;
import com.pizzahut.services.AddressService;

@Controller
@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressService addressService;
	
	//add address
	@PostMapping("/addAddress")
	public ResponseEntity<?> addAddress(@RequestBody AddressDto addressDto)
	{
		try {
			@SuppressWarnings("unused")
			Address newAddress=addressService.addAddress(addressDto);
			return Response.success("Address added");
		}
		catch(Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//get addresses of user
	@GetMapping("/getaddress/{userId}")
	public ResponseEntity<?> getAddress(@PathVariable("userId") int userid ){
		try {
			List<Address> allAddresses = addressService.getAddresses(userid);
			if(!allAddresses.isEmpty()) {
				return Response.success(allAddresses);
			}else {
				return Response.error("Something went wrong");
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
		
	}
	
	//delete by addressId
	@DeleteMapping("/deleteById/{addressId}")
	public ResponseEntity<?> deleteAddress(@PathVariable("addressId") int addressId)
	{
		int count=addressService.deleteByAddressId(addressId);
		if(count==1) {
			return Response.success("Deleted");
		}else
			return Response.error("id not found");
		
	}
	
	//update by addressId
	@PutMapping("/{addressId}")
	public ResponseEntity<?> editAddress(@PathVariable ("addressId") int addressId,
			@RequestBody AddressDto addressDto)
	{
		try {
			System.out.println("===>"+addressDto);
		Address editAddress=addressService.editAddress(addressId, addressDto);
		System.out.println("Hi everyone");
		if(editAddress!=null) {
			return Response.success("Address updated");
		}else {
			return Response.error("Something went wrong");
		}
		
	}catch(Exception e) {
		return Response.error(e.getMessage());
	}
	}

	
}
