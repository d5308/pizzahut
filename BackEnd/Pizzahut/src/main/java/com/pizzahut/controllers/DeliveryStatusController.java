package com.pizzahut.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.DeliveryDto;
import com.pizzahut.entities.DeliveryStatus;
import com.pizzahut.services.DeliveryStatusService;

@Controller
@RestController
@RequestMapping("/DeliveryStatus")
public class DeliveryStatusController {

	@Autowired
	private DeliveryStatusService statusService;
	
	@GetMapping("/{deliveryId}")
	public ResponseEntity<?> viewDeliveryStatus(@PathVariable("deliveryId") int deliveryId)
	{
		try {
			System.out.println("In controller");
			DeliveryStatus result=statusService.findByDeliveryId(deliveryId);
			System.out.println(result);
			if(result==null)
				return Response.error("No result found");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> adddelivery(@RequestBody DeliveryDto deliveryDto)
	{
		try {
			@SuppressWarnings("unused")
			DeliveryStatus newDelivery=statusService.addForDelivery(deliveryDto);
			return Response.success("Delivery added");
		} catch (Exception e) {
			
			return Response.error(e.getMessage());
		}
	}
}
