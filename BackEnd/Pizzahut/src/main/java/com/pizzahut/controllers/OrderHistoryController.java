package com.pizzahut.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.OrderHistoryDto;
import com.pizzahut.entities.OrderHistory;
import com.pizzahut.services.OrderHistoryService;

@Controller
@RestController
@RequestMapping("/orderHistory")
public class OrderHistoryController {

	@Autowired
	private OrderHistoryService historyService;
	
	
	@GetMapping("/orders")
	public ResponseEntity<?> findOrderHistory()
	{
		try {
			List<OrderHistory> checkOrders=historyService.findAll();
			if(checkOrders==null)
				return Response.error("You have not ordered yet,please order");
			return Response.success(checkOrders);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@PostMapping("/addToHistory")
	public ResponseEntity<?> addToHistory(@RequestBody OrderHistoryDto historyDto)
	{
		try {
			@SuppressWarnings("unused")
			OrderHistory newToHistory=historyService.addHistory(historyDto);
			return Response.success("History updated");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@DeleteMapping("/{orderHistoryId}")
	public ResponseEntity<?> deleteOrderHistory(@PathVariable("orderHistoryId")int orderHistoryId)
	{
		int count=historyService.deleteHistory(orderHistoryId);
		if(count==1) {
			return Response.success("deleted successfully!!!");
		}else
			return Response.error("id not found");
	}
 	
	
}
