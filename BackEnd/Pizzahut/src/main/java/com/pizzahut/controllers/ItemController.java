package com.pizzahut.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.entities.Item;
import com.pizzahut.entities.Toppings;
import com.pizzahut.services.ItemService;

@Controller
@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	//get all items in the list
	@GetMapping("/showAll")
	public ResponseEntity<?> findAllItems()
	{
		try {
			List<Item> checkItem=itemService.findAllItem();
			if(checkItem==null)
				return Response.error("Empty");
			return Response.success(checkItem);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//get items by itemId
	@GetMapping("/{itemId}")
	public ResponseEntity<?> findByItemId(@PathVariable ("itemId") int itemId)
	{
		try {
			System.out.println("In controller");
			Item result=itemService.findByItemId(itemId);
			System.out.println(result);
			if(result==null)
				return Response.error("No result found");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//get items by it's type
	@GetMapping("/byType/{type}")
	public ResponseEntity<?> findByType(@PathVariable ("type") String type)
	{
		try {
			System.out.println("In controller");
			List<Item> result=itemService.findByType(type);
			System.out.println(result);
			if(result==null)
				return Response.error("Not found");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
