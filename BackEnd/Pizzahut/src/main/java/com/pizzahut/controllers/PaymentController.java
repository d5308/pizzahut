package com.pizzahut.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.PaymentDto;
import com.pizzahut.entities.Payments;
import com.pizzahut.services.PaymentService;

@Controller
@RestController
@RequestMapping("/payment")
public class PaymentController {

	
	@Autowired
	private PaymentService payService;
	
	@PostMapping("/addPayment")
	public ResponseEntity<?> addPayments(@RequestBody PaymentDto paymentDto)
	{
		try {
			@SuppressWarnings("unused")
			Payments newPayment=payService.addPayments(paymentDto);
			return Response.success("Payments added");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@GetMapping("/showCurrent/{payId}")
	public ResponseEntity<?> showCurrentPayment(@PathVariable("payId")int payId)
	{
		try {
			Payments curPayment=payService.findPayments(payId);
			System.out.println(curPayment);
			if(curPayment==null)
				return Response.error("No result found");
			return Response.success(curPayment);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
}
