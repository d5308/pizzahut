package com.pizzahut.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.CredentialDto;
import com.pizzahut.dtos.UserDto;
import com.pizzahut.entities.Users;
import com.pizzahut.services.UserService;

@CrossOrigin(origins = "*")
@RequestMapping("/user")
@RestController
public class UserController {
	@Autowired
	private UserService userService;

	
	//get details by email
	@GetMapping("/{email}")
	public ResponseEntity<?> findById(@PathVariable("email") String email) {
		try {
			Users result = userService.searchByEmail(email);
			if (result == null) {
				
				return Response.error("Not found");
				
			}
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// API for sign up of User/Admin
	@PostMapping("/signup")
	public ResponseEntity<?> SignUpUser(@RequestBody Users user){
		try {
			Users newUserEmailVerify=userService.searchByEmail(user.getEmail());
			Users newUserPhVerify=userService.searchByPhone(user.getPhoneNo());
			System.out.println(newUserEmailVerify);
			System.out.println(newUserPhVerify);
			if(newUserEmailVerify==null && newUserPhVerify==null) {
				Users add=userService.addUser(user);
				return Response.success(add);
			}else if(newUserEmailVerify !=null) {
				return Response.error("Email already exist");
			}else if(newUserPhVerify != null) {
				return Response.error("Phone Number already exists");
			}else {
				return Response.error("Something went wrong");
			}
			
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// API for sign in of User/Admin

	@PostMapping("/signin")
	public ResponseEntity<?> signInUser(@Valid @RequestBody CredentialDto cred)
	{
		Users user=userService.getUser(cred);
		if(user==null)
			return Response.error("User not found");
		return Response.success(user);
	}
	
	//update user details
	@PutMapping("/{userId}")
	public ResponseEntity<?> editUsers(@PathVariable("userId")int userId,
			@RequestBody UserDto userdto)
	{
		try {
			Users editUsers=userService.updateUser(userId, userdto);
			if(editUsers!=null) {
				return Response.success("Updated");
			}else {
				return Response.error("Something went wrong");
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}	

}
