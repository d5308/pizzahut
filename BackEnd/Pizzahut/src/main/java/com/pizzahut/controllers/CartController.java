package com.pizzahut.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.CartDto;
import com.pizzahut.entities.Cart;
import com.pizzahut.services.CartService;

@Controller
@RestController
@RequestMapping("/cart")
public class CartController {

	@Autowired
	private CartService cartService;
	
	//add cart
	@PostMapping("/add")
	public ResponseEntity<?> addToCart(@RequestBody CartDto cartdto){
		try {
			System.out.println(cartdto);
			Cart addTocart = cartService.addTocart(cartdto);
			if(addTocart != null) {
				return Response.success("Cart added successfully");
			}else {
				return Response.error("Something went wrong");
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	  //delete by id
	@DeleteMapping("/deleteById/{cartId}")
	public ResponseEntity<?> deleteCart(@PathVariable("cartId") int cartId)
	{
		int count=cartService.deleteByCartId(cartId);
		if(count==1) {
			return Response.success("Deleted");
		}else
			return Response.error("id not found");
		
	}
}
