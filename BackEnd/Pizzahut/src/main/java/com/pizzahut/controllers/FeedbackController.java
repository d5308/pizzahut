package com.pizzahut.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizzahut.dtos.FeedbackDto;
import com.pizzahut.entities.Feedback;
import com.pizzahut.services.FeedBackService;

@Controller
@RestController
@RequestMapping("/feedback")
public class FeedbackController {

	@Autowired
	private FeedBackService feedBackService;
	
	//add new feedback to list
	@PostMapping("/addFeedback")
	public ResponseEntity<?> addFeedback(@RequestBody FeedbackDto feedbackDto)
	{
		try {
			@SuppressWarnings("unused")
			Feedback newFeed=feedBackService.addNewFeedback(feedbackDto);
			return Response.success("Feedback added");
			
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//delete a particular feedback by id
	@DeleteMapping("/deleteById/{feedbackId}")
	public ResponseEntity<?> deletefeedbacks(@PathVariable("feedbackId") int feedbackId)
	{
		int count=feedBackService.deleteFeedback(feedbackId);
		if(count==1) {
			return Response.success("Deleted");
		}else
			return Response.error("id not found");
		
	}
	
	//edit particular feedback by id
//	@PutMapping("/update/{feedbackId}")
//	public ResponseEntity<?> updateFeedback(@Valid @PathVariable ("feedbackId") int feedbackId,
//			@RequestBody FeedbackDto feedbackDto)
//	{
//		try {
//			Feedback editFeedback=feedBackService.editFeedback(feedbackId, feedbackDto);
//			if(editFeedback!=null) {
//				return Response.success("Updated");
//			}else
//			{
//				return Response.error("error");
//			}
//		} catch (Exception e) {
//			return Response.error(e.getMessage());
//		}
//	}
}
