package com.pizzahut.dtos;

public class ToppingDto {

private int toppingId;
	
	private int pizzaId;
	
	private String topping;
	
	private double price;
	
	public ToppingDto() {
		// TODO Auto-generated constructor stub
	}

	public ToppingDto(int toppingId, int pizzaId, String topping, double price) {
		super();
		this.toppingId = toppingId;
		this.pizzaId = pizzaId;
		this.topping = topping;
		this.price = price;
	}

	public int getToppingId() {
		return toppingId;
	}

	public void setToppingId(int toppingId) {
		this.toppingId = toppingId;
	}

	public int getPizzaId() {
		return pizzaId;
	}

	public void setPizzaId(int pizzaId) {
		this.pizzaId = pizzaId;
	}

	public String getTopping() {
		return topping;
	}

	public void setTopping(String topping) {
		this.topping = topping;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return String.format("ToppingDto [toppingId=%s, pizzaId=%s, topping=%s, price=%s]", toppingId, pizzaId, topping,
				price);
	}
	
}
