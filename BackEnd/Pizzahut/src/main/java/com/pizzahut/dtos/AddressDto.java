package com.pizzahut.dtos;

import com.pizzahut.entities.Users;

public class AddressDto {

	
	
	private int userId;
	
	private String plotNo;
	private String streetName;
	private String city;
	private String district;
	private String soverignState;
	private String pinCode;

	public AddressDto() {
		// TODO Auto-generated constructor stub
	}

	public AddressDto(int userId, String plotNo, String streetName, String city, String district, String soverignState,
			String pinCode) {
		super();
		this.userId = userId;
		this.plotNo = plotNo;
		this.streetName = streetName;
		this.city = city;
		this.district = district;
		this.soverignState = soverignState;
		this.pinCode = pinCode;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getSoverignState() {
		return soverignState;
	}

	public void setSoverignState(String soverignState) {
		this.soverignState = soverignState;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public String toString() {
		return String.format(
				"AddressDto [userId=%s, plotNo=%s, streetName=%s, city=%s, district=%s, soverignState=%s, pinCode=%s]",
				userId, plotNo, streetName, city, district, soverignState, pinCode);
	}

	
}
