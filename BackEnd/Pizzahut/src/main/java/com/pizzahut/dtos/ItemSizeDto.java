package com.pizzahut.dtos;

public class ItemSizeDto {

	private int sizeId;
	private int itemId;
	private String size;
	private double price;
	
	public ItemSizeDto() {
		// TODO Auto-generated constructor stub
	}

	public ItemSizeDto(int sizeId, int itemId, String size, double price) {
		super();
		this.sizeId = sizeId;
		this.itemId = itemId;
		this.size = size;
		this.price = price;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return String.format("ItemSizeDto [sizeId=%s, itemId=%s, size=%s, price=%s]", sizeId, itemId, size, price);
	}
	
}
