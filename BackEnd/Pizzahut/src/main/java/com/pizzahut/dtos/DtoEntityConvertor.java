package com.pizzahut.dtos;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartException;

import com.pizzahut.entities.Address;
import com.pizzahut.entities.Cart;
import com.pizzahut.entities.DeliveryStatus;
import com.pizzahut.entities.Feedback;
import com.pizzahut.entities.Item;
import com.pizzahut.entities.ItemImage;
import com.pizzahut.entities.ItemSize;
import com.pizzahut.entities.OrderHistory;
import com.pizzahut.entities.Payments;
import com.pizzahut.entities.Toppings;
import com.pizzahut.entities.Users;
import com.pizzahut.entities.ToppingImages;

@Component
public class DtoEntityConvertor {
	
	//userdto to user
		public UserDto toUserDto(Users entity) {
			UserDto dto = new UserDto();
			dto.setUserId(entity.getUserId());
			dto.setFirstName(entity.getFirstName());
			dto.setLastName(entity.getLastName());
			dto.setEmail(entity.getEmail());
			dto.setPassword(entity.getPassword());
			return dto;
		}

		//user to userdto
		public Users toUserEntity(UserDto dto) {
			Users entity = new Users();
			entity.setUserId(dto.getUserId());
			entity.setFirstName(dto.getFirstName());
			entity.setLastName(dto.getLastName());
			entity.setEmail(dto.getEmail());
			entity.setPassword(dto.getPassword());
			entity.setRole(dto.getRole());
			entity.setPhoneNo(dto.getPhoneNo());
			return entity;		
		}
		
		//for address to addressdto
	public Address toAddressEntity(AddressDto addressDto) {
		System.out.println("In convertor");
		Address convertedAddress = new Address();
		
		Users useraddress = new Users();
		useraddress.setUserId(addressDto.getUserId());
//		useraddress.setPassword();
		
		convertedAddress.setUsers(useraddress);
		
		convertedAddress.setPlotNo(addressDto.getPlotNo());
		convertedAddress.setStreetName(addressDto.getStreetName());
		convertedAddress.setCity(addressDto.getCity());
		convertedAddress.setDistrict(addressDto.getDistrict());
		convertedAddress.setSoverignState(addressDto.getSoverignState());
		convertedAddress.setPinCode(addressDto.getPinCode());
		return convertedAddress;
	}
	
	//from feedbackdto to feedback
	
		public Feedback toFeedBackEntity(FeedbackDto dto)
		{
			System.out.println("In convertor");
			Feedback convertFeedback=new Feedback();
			Users userFeedback=new Users();
			userFeedback.setUserId(dto.getUserId());
			convertFeedback.setUser(userFeedback);
			convertFeedback.setFeedback(dto.getFeedback());
			//convertFeedback.setFeedBackTime(dto.getFeedBackTime());
			return convertFeedback;
		}
		
	
	
	//from cart to cartdto
	public Cart tocartEntity(CartDto cartDto)
	{
		System.out.println("In convertor");
		Cart convertedCart=new Cart();
		Users userCart = new Users();
		ItemSize itemCart=new ItemSize();
		Toppings toppingsCart=new Toppings();
		
		userCart.setUserId(cartDto.getUserId());
		convertedCart.setUser(userCart);
		
		
		itemCart.setSizeId(cartDto.getSizeId());
		convertedCart.setItemSize(itemCart);
		
		toppingsCart.setToppingId(cartDto.getToppingId());
		convertedCart.setToppings(toppingsCart);
		
		
		convertedCart.setQuantity(cartDto.getQuantity());
		convertedCart.setPrice(cartDto.getPrice());

		return convertedCart;	
	}
	
	
	public DeliveryStatus toDelivery(DeliveryDto dto)
	{
		System.out.println("In convertor");
		DeliveryStatus convertDelivery=new DeliveryStatus();
		Payments convertPay=new Payments();
		convertPay.setPayId(dto.getPayId());
		convertDelivery.setPayments(convertPay);
		convertDelivery.setDeliveryStatus(dto.getDeliveryStatus());
		convertDelivery.setDeliveryTime(dto.getDeliveryTime());
		return convertDelivery;
	}
	
	
	
	public ToppingImages toToppingImageEntity(ToppingImageDto toppingImageDto,int toppingId) {
		System.out.println("In convertor");
		if(toppingImageDto==null)
			return null;
		ToppingImages toppingImg=new ToppingImages();
		Toppings topping=new Toppings();
		topping.setToppingId(toppingId);
		toppingImg.setToppingImgId(toppingId);
		toppingImg.setToppings(topping);
		toppingImg.setTopTime(toppingImageDto.getTopTime());
		System.out.println(toppingImageDto.getTopTime());
		try {
			toppingImg.setData(toppingImageDto.getData().getBytes());
		} catch (Exception e) {
			throw new MultipartException("Can't convert MultipartFile to bytes : " + toppingImageDto.getData(), e);

		}
		return toppingImg;
	}
	
	public ItemImage toItemEntity(ItemImgFormDto itemDto, int itemId) {
		System.out.println("in convertor before");
		if(itemDto == null)
			return null;
		ItemImage itemImg = new ItemImage();
		Item item = new Item();
		item.setItemId(itemId);
		itemImg.setItem(item);
		itemImg.setTime(itemDto.getTime());
		System.out.println(itemDto.getTime());
		try {
			itemImg.setData(itemDto.getData().getBytes());
		} catch (Exception e) {
			throw new MultipartException("Can't convert MultipartFile to bytes : " + itemDto.getData(), e);
		}
		return itemImg;
	}
	
	public ToppingImageDto toToppingImg(ToppingImages img)
	{
		if(img==null)
			return null;
		ToppingImageDto dto=new ToppingImageDto();
		BeanUtils.copyProperties(img, dto);
		return dto;
	}
	
	public Payments toPaymentEntity(PaymentDto paymentDto, int totalamount)
	{
		Payments convertPayment=new Payments();
		Users user=new Users();
		user.setUserId(paymentDto.getUserId());
		convertPayment.setPayStatus(paymentDto.getPayStatus());
		convertPayment.setUser(user);
		convertPayment.setMode(paymentDto.getMode());
		convertPayment.setTotalAmount(totalamount);
		convertPayment.setPayTimeStamp(paymentDto.getPayTimeStamp());
		return convertPayment;	
	}	
	

	
	public OrderHistory toHistoryEntity(OrderHistoryDto historyDto)
	{
		OrderHistory convertHistory=new OrderHistory();
		Payments payment=new Payments();
		Address addressConvert=new Address();
		DeliveryStatus convertStatus=new DeliveryStatus();
		payment.setPayId(historyDto.getPayId());
		convertHistory.setPayment(payment);
		convertHistory.setUserId(historyDto.getUserId());
		convertHistory.setSizeId(historyDto.getSizeId());
		convertHistory.setToppingId(historyDto.getToppingId());
		convertHistory.setQuantity(historyDto.getQuantity());
		convertHistory.setTotalPrice(historyDto.getTotalPrice());
		
		convertStatus.setDeliveryId(historyDto.getDeliveryId());
		convertHistory.setDelivery(convertStatus);
		
		addressConvert.setAddressId(historyDto.getAddressId());
		convertHistory.setAddresses(addressConvert);
		return convertHistory;
		
	}
}
