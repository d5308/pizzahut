package com.pizzahut.dtos;

public class ItemDto {

	private int itemId;
	
	private String type;
	
	private String itemName;
	
	private String description;
	
	public ItemDto() {
		// TODO Auto-generated constructor stub
	}

	public ItemDto(int itemId, String type, String itemName, String description) {
		super();
		this.itemId = itemId;
		this.type = type;
		this.itemName = itemName;
		this.description = description;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return String.format("ItemDto [itemId=%s, type=%s, itemName=%s, description=%s]", itemId, type, itemName,
				description);
	}
	
}
