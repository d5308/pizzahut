package com.pizzahut.dtos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.web.multipart.MultipartFile;

import com.pizzahut.entities.Toppings;

public class ToppingImageDto {

	
	private int toppingId;
	private Toppings toppings;
	private MultipartFile data;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date topTime; 
	
	public ToppingImageDto() {
		// TODO Auto-generated constructor stub
	}

	public ToppingImageDto(int toppingId, Toppings toppings, MultipartFile data, Date topTime) {
		super();
		this.toppingId = toppingId;
		this.toppings = toppings;
		this.data = data;
		this.topTime = topTime;
	}

	public int getToppingId() {
		return toppingId;
	}

	public void setToppingId(int toppingId) {
		this.toppingId = toppingId;
	}

	public Toppings getToppings() {
		return toppings;
	}

	public void setToppings(Toppings toppings) {
		this.toppings = toppings;
	}

	public MultipartFile getData() {
		return data;
	}

	public void setData(MultipartFile data) {
		this.data = data;
	}

	public Date getTopTime() {
		return topTime;
	}

	public void setTopTime(Date topTime) {
		this.topTime = topTime;
	}

	@Override
	public String toString() {
		return String.format("ToppingImageDto [toppingId=%s, toppings=%s, data=%s, topTime=%s]", toppingId, toppings,
				data, topTime);
	}

	
}
