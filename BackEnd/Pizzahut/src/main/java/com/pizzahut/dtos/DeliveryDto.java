package com.pizzahut.dtos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class DeliveryDto {

	
	private int payId;
	private String deliveryStatus;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date deliveryTime;
	
	public DeliveryDto() {
		// TODO Auto-generated constructor stub
	}

	public DeliveryDto( int payId, String deliveryStatus, Date deliveryTime) {
		super();
		
		this.payId = payId;
		this.deliveryStatus = deliveryStatus;
		this.deliveryTime = deliveryTime;
	}

	public int getPayId() {
		return payId;
	}

	public void setPayId(int payId) {
		this.payId = payId;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	@Override
	public String toString() {
		return String.format("DeliveryDto [deliveryId=%s, payId=%s, deliveryStatus=%s, deliveryTime=%s]",
				payId, deliveryStatus, deliveryTime);
	}
	
	
	
}
