package com.pizzahut.dtos;


import com.pizzahut.entities.Users;

public class FeedbackDto {

	private int feedbackId;
	private Users user;
	private int userId;
	private String feedback;
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(insertable = false)
//	private Date feedBackTime;
	
	public FeedbackDto() {
		// TODO Auto-generated constructor stub
	}

	public FeedbackDto(int feedbackId, Users user, int userId, String feedback) {
		super();
		this.feedbackId = feedbackId;
		this.user = user;
		this.userId = userId;
		this.feedback = feedback;
	}

	public int getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(int feedbackId) {
		this.feedbackId = feedbackId;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	@Override
	public String toString() {
		return String.format("FeedbackDto [feedbackId=%s, user=%s, userId=%s, feedback=%s]", feedbackId, user, userId,
				feedback);
	}
	
	
}
