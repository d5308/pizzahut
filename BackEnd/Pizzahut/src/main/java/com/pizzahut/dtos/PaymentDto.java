package com.pizzahut.dtos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PaymentDto {

	private int userId;
//	private double totalAmount;
	private String payStatus;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date payTimeStamp;
	private String mode;
	
	public PaymentDto() {
		// TODO Auto-generated constructor stub
	}

	public PaymentDto(int userId, String payStatus, Date payTimeStamp, String mode) {
		super();
		this.userId = userId;
		this.payStatus = payStatus;
		this.payTimeStamp = payTimeStamp;
		this.mode = mode;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public Date getPayTimeStamp() {
		return payTimeStamp;
	}

	public void setPayTimeStamp(Date payTimeStamp) {
		this.payTimeStamp = payTimeStamp;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return String.format("PaymentDto [userId=%s, payStatus=%s, payTimeStamp=%s, mode=%s]", userId, payStatus,
				payTimeStamp, mode);
	}

	
}
