package com.pizzahut.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.Item;
import com.pizzahut.entities.Toppings;

public interface ItemDao extends JpaRepository<Item, Integer>{

	Item findByItemId(int itemId);
	List<Item> findByType(String type);
	
}
