package com.pizzahut.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.dtos.AddressDto;
import com.pizzahut.entities.Address;
import com.pizzahut.entities.Users;



public interface AddressDao extends JpaRepository<Address, Integer> {
	List<Address> findByUsers(Users user);
	
}