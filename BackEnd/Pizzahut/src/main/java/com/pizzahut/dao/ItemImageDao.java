package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.Item;
import com.pizzahut.entities.ItemImage;

public interface ItemImageDao extends JpaRepository<ItemImage, Integer>{
	ItemImage findByItem(Item item);
}
