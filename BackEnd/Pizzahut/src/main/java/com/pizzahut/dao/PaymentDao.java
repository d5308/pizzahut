package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.Payments;

public interface PaymentDao extends JpaRepository<Payments, Integer>{

}
