package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pizzahut.entities.Cart;

public interface CartDao extends JpaRepository<Cart,Integer>{

	//@Query(value="select ")
	@Query(value = "select sum(price*quantity) as totalamount from cart where userid=?1", nativeQuery = true)
	int findTotalAmount(int userid);
	
}
