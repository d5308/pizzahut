package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pizzahut.entities.ItemSize;

public interface ItemSizeDao extends JpaRepository<ItemSize	, Integer>{

//	List<ItemSize> findByItem(Item item);
//	List<ItemSize> findBySizeId(int sizeId);
	
	@Query(value="SELECT * from itemSize WHERE size=?1 and itemId=?2",nativeQuery = true)
	   ItemSize getSizeOfPizza(String size,Integer itemId);
}
