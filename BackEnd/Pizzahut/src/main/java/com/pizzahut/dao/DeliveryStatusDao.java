package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.DeliveryStatus;

public interface DeliveryStatusDao extends JpaRepository<DeliveryStatus	,Integer>{

}
