package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.Users;

public interface UserDao extends JpaRepository<Users, Integer>{
	Users findByEmail(String email);
	Users findByPhoneNo(String phoneNo);
//	Users findByUserId(int userId);
	
}
