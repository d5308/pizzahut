package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.OrderHistory;

public interface OrderHistoryDao extends JpaRepository<OrderHistory,Integer>{

}
