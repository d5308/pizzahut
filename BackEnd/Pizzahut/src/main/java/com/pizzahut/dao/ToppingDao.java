package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.Item;
import com.pizzahut.entities.Toppings;

public interface ToppingDao extends JpaRepository<Toppings, Integer>{

	Toppings findByToppingId(int toppingId);
}
