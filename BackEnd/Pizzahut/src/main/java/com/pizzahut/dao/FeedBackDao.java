package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pizzahut.entities.Feedback;

public interface FeedBackDao extends JpaRepository<Feedback, Integer>{

	
}
