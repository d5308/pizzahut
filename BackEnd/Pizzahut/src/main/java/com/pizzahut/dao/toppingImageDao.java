package com.pizzahut.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizzahut.entities.ToppingImages;

public interface toppingImageDao extends JpaRepository<ToppingImages,Integer>{

	ToppingImages findByToppingImgId(int toppingImgId);
}
