package com.pizzahut.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.toppingImageDao;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.ToppingImageDto;
import com.pizzahut.entities.ToppingImages;


@Service
@Transactional
public class toppingImgService {

	@Autowired
	private toppingImageDao topImgDao;
	@Autowired
	private DtoEntityConvertor convertor;
	
	public Map<String, Object> addToppingImage(int toppingId,ToppingImageDto dto)
	{
		ToppingImages top=convertor.toToppingImageEntity(dto, toppingId);
		top=topImgDao.save(top);
		return Collections.singletonMap("insertedImage", top.getToppings());
	}
	

	public ToppingImages findByImageId(int toppingImgId)
	{
		ToppingImages result=topImgDao.findByToppingImgId(toppingImgId);
		System.out.println(result);
		return result;
	}
	
//	public List<ToppingImages> showAll()
//	{
//		return topImgDao.findAll();
//	}
	
}
