package com.pizzahut.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.FeedBackDao;
import com.pizzahut.dao.UserDao;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.FeedbackDto;
import com.pizzahut.entities.Feedback;
import com.pizzahut.entities.Users;

@Service
@Transactional
public class FeedBackService {

	@Autowired
	private FeedBackDao feedbackDao;
	@Autowired
	private DtoEntityConvertor convertor;
	@Autowired
	private UserDao userDao;

	//add new feedback to list
	public Feedback addNewFeedback(FeedbackDto feedbackDto) {
		Feedback add = convertor.toFeedBackEntity(feedbackDto);
		feedbackDao.save(add);
		return add;
	}

	//delete a particular feedback by id
	public int deleteFeedback(int feedbackId) {
		feedbackDao.deleteById(feedbackId);
		return 1;
	}

	//edit particular feedback by id
//	public Feedback editFeedback(int feedbackId, FeedbackDto editfeeds) {
//		Feedback checkId = feedbackDao.getById(feedbackId);
//		System.out.println(checkId);
//		if (checkId != null) {
//			Feedback updateFeedback = convertor.toFeedBackEntity(editfeeds);
//			updateFeedback.setFeedbackId(checkId.getFeedbackId());
//			feedbackDao.save(updateFeedback);
//			return updateFeedback;
//		}
//		return null;
//	}
}
