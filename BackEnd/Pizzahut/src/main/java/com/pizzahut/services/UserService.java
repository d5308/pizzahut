package com.pizzahut.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.UserDao;
import com.pizzahut.dtos.CredentialDto;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.UserDto;
import com.pizzahut.entities.Users;

@Service
@Transactional
public class UserService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private DtoEntityConvertor convertor;
	@Autowired
	private PasswordEncoder encoder;
	
	//to check user email
	public Users searchByEmail(String email) {
		Users emailCheck = userDao.findByEmail(email);
		if(emailCheck == null)
			return null;
		else 
			return emailCheck;
	}
	
	//to check user mobile
	public Users searchByPhone(String phNumber) {
		Users phCheck = userDao.findByPhoneNo(phNumber);
		if(phCheck == null)
			return null;
		else 
			return phCheck;
	}
	
	//to add user
	public Users addUser(Users user){
		String encodedPassword=encoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		userDao.save(user);
		return user;
	}
	
	//to get user for sign in
	public Users getUser(CredentialDto cred) {
		Users dbUser = userDao.findByEmail(cred.getEmail());
		String rawPassword=cred.getPassword();
		if(dbUser != null && encoder.matches(rawPassword,dbUser.getPassword() )) 
		{
			return dbUser;
		}else
			return null;
	}
	
	
	//update user details
	public Users updateUser(int userId,UserDto dto)
	{
		Users checkId=userDao.getById(userId);
		System.out.println(checkId);
		if(checkId!=null)
		{
			Users updateUsers=convertor.toUserEntity(dto);
			String encodedPassword=encoder.encode(updateUsers.getPassword());
			updateUsers.setPassword(encodedPassword);
			updateUsers.setUserId(checkId.getUserId());
			userDao.save(updateUsers);
			return updateUsers;
		}
		return null;
	}
}
