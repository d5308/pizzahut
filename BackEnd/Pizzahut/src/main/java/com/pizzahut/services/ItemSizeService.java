package com.pizzahut.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.ItemSizeDao;
import com.pizzahut.entities.ItemSize;

@Service
@Transactional
public class ItemSizeService {

	@Autowired
	private ItemSizeDao itemSizeDao;
	
//	public List<ItemSize> getItem(int itemId){
//		Item getItemId= new Item();
//		getItemId.setItemId(itemId);
//		List<ItemSize> getCertainItemSize =itemSizeDao.findByItem(getItemId);
//			return getCertainItemSize;
//	}
	
	//show by sizeId
		public ItemSize SizeOfPizza(String size, int itemId){
			ItemSize thisSize =itemSizeDao.getSizeOfPizza(size, itemId);
			return thisSize;
		}
	
}
