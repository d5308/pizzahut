package com.pizzahut.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.ItemDao;
import com.pizzahut.entities.Item;

@Service
@Transactional
public class ItemService {

	@Autowired
	private ItemDao itemDao;
	
	//show all items in the list
	public List<Item> findAllItem( )
	{
		return itemDao.findAll();
	}
	
	//show selected item by it's Id
	public Item findByItemId(int itemId)
	{
		Item item= itemDao.getById(itemId);
		System.out.println(item);
		return item;
		
	}
	
	//get by type
	public List<Item> findByType(String type)
	{
		List<Item> listByType=itemDao.findByType(type);
		if(listByType!=null) {
			return listByType;
		}
		return null;
	}
}
