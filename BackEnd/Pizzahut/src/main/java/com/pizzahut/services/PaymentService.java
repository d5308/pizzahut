package com.pizzahut.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.CartDao;
import com.pizzahut.dao.PaymentDao;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.PaymentDto;
import com.pizzahut.entities.Payments;

@Service
@Transactional
public class PaymentService {

//	@Autowired
//	private PaymentDto paymentDto;
	@Autowired
	private DtoEntityConvertor convertor;
	@Autowired
	private PaymentDao paymentDao;
	@Autowired
	private CartDao cartDao;
	
	public Payments addPayments(PaymentDto paymentDto)
	{
		int totalAmount = cartDao.findTotalAmount(paymentDto.getUserId());
		System.out.println(totalAmount);
		Payments add=convertor.toPaymentEntity(paymentDto,totalAmount);
		paymentDao.save(add);
		return add;
	}
	
	public Payments findPayments(int payId)
	{
		Payments togetPayments=paymentDao.getById(payId);
		return togetPayments;
		
	}
}
