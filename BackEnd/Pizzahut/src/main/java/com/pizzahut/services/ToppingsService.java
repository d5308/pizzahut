package com.pizzahut.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.ToppingDao;
import com.pizzahut.dtos.ToppingDto;
import com.pizzahut.entities.Toppings;

@Service
@Transactional
public class ToppingsService {

	@Autowired
	private ToppingDao toppingDao;
	
	//get all toppings
	public List<Toppings> findAllToppings(){
		
		return toppingDao.findAll(); 
	}
	
	//get by id
	public Toppings findByToppingId(int toppingId)
	{
		Toppings toppings=toppingDao.getById(toppingId);
		return toppings;
	}
}
