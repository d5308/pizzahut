package com.pizzahut.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.DeliveryStatusDao;
import com.pizzahut.dtos.DeliveryDto;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.entities.DeliveryStatus;

@Service
@Transactional
public class DeliveryStatusService {

	@Autowired
	private DeliveryStatusDao statusDao;
	@Autowired
	private DtoEntityConvertor convertor;
	
	public DeliveryStatus findByDeliveryId(int deliveryId)
	{
		DeliveryStatus status=statusDao.getById(deliveryId);
		return status;
	}
	
	public DeliveryStatus addForDelivery(DeliveryDto deliveryDto)
	{
		DeliveryStatus add=convertor.toDelivery(deliveryDto);
		statusDao.save(add);
		return add;
	}
}
