package com.pizzahut.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.OrderHistoryDao;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.dtos.OrderHistoryDto;
import com.pizzahut.entities.OrderHistory;

@Service
@Transactional
public class OrderHistoryService {

	@Autowired
	private OrderHistoryDao historyDao;
	@Autowired
	private DtoEntityConvertor convertor;
	
	public List<OrderHistory> findAll()
	{
		List<OrderHistory> list=historyDao.findAll();
		return list;
	}
	
	public OrderHistory addHistory(OrderHistoryDto historyDto)
	{
		OrderHistory add=convertor.toHistoryEntity(historyDto);
		historyDao.save(add);
		return add;
	}
	
	public int deleteHistory(int orderHistoryId)
	{
		historyDao.deleteById(orderHistoryId);
		return 1;
	}
}
