package com.pizzahut.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.CartDao;
import com.pizzahut.dtos.CartDto;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.entities.Cart;

@Transactional
@Service
public class CartService {

	@Autowired
	private CartDao cartDao;
	
	
	@Autowired
	   private DtoEntityConvertor convertor;
	       
	//add cart
	   public Cart addTocart(CartDto cartDto) {
	       Cart cartAdd = convertor.tocartEntity(cartDto);
	       System.out.println(cartAdd);
	       cartDao.save(cartAdd);
	       return cartAdd;
	   }
	   
	   //delete by id
	public int deleteByCartId(int cartId)
	{
		
		cartDao.deleteById(cartId);
		return 1;
		
	}
	
}
