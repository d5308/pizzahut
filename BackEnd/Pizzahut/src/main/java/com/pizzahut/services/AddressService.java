package com.pizzahut.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pizzahut.dao.AddressDao;
import com.pizzahut.dao.UserDao;
import com.pizzahut.dtos.AddressDto;
import com.pizzahut.dtos.DtoEntityConvertor;
import com.pizzahut.entities.Address;
import com.pizzahut.entities.Users;

@Service
@Transactional
public class AddressService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private AddressDao addressDao;
	@Autowired
	private DtoEntityConvertor convertor;

	
	//add new address
	public Address addAddress(AddressDto addressDto)
	{
		Address add=convertor.toAddressEntity(addressDto);
		System.out.println("NEW address--->"+add);
		addressDao.save(add);
		return add;
	}
	
	// to get addresses of user
		public List<Address> getAddresses(int userid){
			Address toGetAddress = new Address();
			Users getUserAdd= new Users();
			getUserAdd.setUserId(userid);
			toGetAddress.setUsers(getUserAdd);
			List<Address> getFinalAddress = addressDao.findByUsers(getUserAdd);
				return getFinalAddress;
		}
		
	// delete certain address
		

		public int deleteByAddressId(int addressId)
		{
//			int checkId=deleteByAddressId(addressId);
			addressDao.deleteById(addressId);
			return 1;
		}
		
	//update Address
		public Address editAddress(int addressId,AddressDto editAddress)
		{
			Address checkId=addressDao.getById(addressId);
			System.out.println(checkId);
			if(checkId !=null) {
				Address updateAddress=convertor.toAddressEntity(editAddress);
				updateAddress.setUsers(userDao.getById(editAddress.getUserId()));
				updateAddress.setAddressId(checkId.getAddressId());
				System.out.println("----->"+updateAddress);
				addressDao.save(updateAddress);
			return updateAddress;
		}
			return null;
		}
}
