import React from 'react'
import { motion } from 'framer-motion'
import { useNavigate } from 'react-router'
import { Dropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from '../../images/colorLogo.png'
import './Header.css'

export default function Header() {

    const { currentfirstName } = sessionStorage
    const navigate = useNavigate()
    const logout = () => {
        sessionStorage.clear()
        navigate('/')
    }

    const loginstatus = sessionStorage.getItem("currentloginStatus")
    const chechLogin = () => {
        if (loginstatus != 1) {
            var drop = document.getElementById('dropdown-basic')
            drop.disabled = true
        }
    }

    return (
        <div onLoad={chechLogin}>
            <div className="row shadow sticky-top" style={{ backgroundColor: "darkslateblue" }} >
                <div className="col">
                    <img src={logo} alt="" className='headerlogoProfile' onClick={() => (navigate('/'))} />
                    <motion.button className='btn btn-link headerbtn'
                        whileHover={{ scale: 1.03, color: "whitesmoke" }}
                        whileTap={{ scale: 0.98, color: "whitesmoke" }}
                        onClick={() => navigate('/pizza')}
                    >Pizzzas</motion.button>

                    <motion.button className='btn btn-link headerbtn'
                        whileHover={{ scale: 1.03, color: "whitesmoke" }}
                        whileTap={{ scale: 0.98, color: "whitesmoke" }}
                        onClick={() => (navigate('/Vegpizza'))}
                    >Veg-Pizzas</motion.button>

                    <motion.button className='btn btn-link headerbtn'
                        whileHover={{ scale: 1.03, color: "whitesmoke" }}
                        whileTap={{ scale: 0.98, color: "whitesmoke" }}
                        onClick={() => (navigate('/beverages'))}
                    >Beverages</motion.button>
                </div>

                <div className="col">
                    <Dropdown>
                        <Dropdown.Toggle variant="success" id="dropdown-basic" className='float-end dropBtnd'>
                            Hi {currentfirstName} !
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => (navigate('/profile'))}>Profile</Dropdown.Item>
                            <Dropdown.Item onClick={() => navigate('/MyOrders')}>My Orders</Dropdown.Item>
                            <Dropdown.Item href="">Something</Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <motion.button className='float-end cartbtn'
                        whileHover={{ scale: 1.03, backgroundColor: "white" }}
                        onClick={() => (navigate('/cart'))}
                    >Cart</motion.button>
                </div>
            </div>
        </div>
    )
}