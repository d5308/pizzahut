import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import { toast } from 'react-toastify'
import Header from '../Header/Header'
import URL from '../URL/Url'
import './Orderdetails.css'

export default function Orderdetails() {
    const { state } = useLocation()
    const { cartId } = state

    const [orderdetail, setOrderdetail] = useState([])
    const getbycartid = () => {
        const url = `${URL}cart/${cartId}`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            if (result.status === 'success') {
                setOrderdetail(result.data)
            } else {
                toast.error("Something Went wrong at our end. Don't worry, it will be fixed soon ")
            }
        })
    }

    useEffect(() => {
        getbycartid()
    }, [])

    const loginstatus = sessionStorage.getItem("currentloginStatus")
    const loadCart = () => {
        window.scrollTo(0, 0)
        if (loginstatus != 1) {
            var btn = document.getElementById("payid")
            btn.disabled = true
            toast.error("Please add some Items in Your Basket to proceed further")
        }
    }

    return (
        <div style={{ overflowX: "hidden" }} className='fixedcontent' onLoad={loadCart}>
            <Header />
            <div style={{ backgroundColor: "whitesmoke" }}>
                <br />
                <div className=' container shadow-lg' style={{ backgroundColor: "white", minHeight: "500px" }}>
                    <br />
                    <div className='orderdetailContainer'>
                        {orderdetail.map((thisdetail) =>
                            <div>
                                <div className="row">
                                    <div className="col">
                                        <h4>Delivery Address Details</h4> <hr />
                                        <div className='addresscontainer'>
                                            <h6>Plot No :     {thisdetail.delId.address.plotNo}</h6>
                                            <h6>Street Name : {thisdetail.delId.address.streetName}</h6>
                                            <h6>City :        {thisdetail.delId.address.city}</h6>
                                            <h6>District :    {thisdetail.delId.address.district}</h6>
                                            <h6>State :       {thisdetail.delId.address.soverignState}</h6>
                                            <h6>Pincode :     {thisdetail.delId.address.pincode}</h6>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <h4>Payment Details</h4> <hr />
                                        <div className='addresscontainer'>
                                            <h6>Payment Id :        {thisdetail.delId.payments.payId}</h6>
                                            <h6>Payment status :     {thisdetail.delId.payments.payStatus}</h6>
                                            <h6>Payment mode : {thisdetail.delId.payments.mode}</h6>
                                        </div> <hr />
                                        <div className='addresscontainer'>
                                            <h6>Delivery status :        {thisdetail.delId.deliveryStatus}</h6>
                                        </div>
                                    </div>
                                </div> 
                                <br />
                                <h4>Order Details</h4> <hr />
                                <div className='tableContainer shadow'>
                                    <div style={{ padding: "0.3rem" }}>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div className='imageBlock'>
                                                            <img src={URL + 'itemImage/item/' + thisdetail.itemsize.item.itemid} alt="" style={{ marginLeft: "10px", borderRadius: "20px", width: "150px", height: "100px" }} />
                                                        </div>
                                                    </td>
                                                    <td style={{ width: "600px" }}>
                                                        <div className="row">
                                                            <div className="col">
                                                                <h4 className='cartInfo' style={{ marginLeft: "10%" }}>
                                                                    {thisdetail.itemsize.item.itemName}
                                                                </h4>
                                                            </div>
                                                            <div className="col">
                                                                <h4 className='cartInfo' style={{ marginRight: "20px" }}>Rs. {thisdetail.price}</h4>
                                                            </div>
                                                        </div>
                                                        <h6 className='cartInfo' style={{ color: "gray" }}>{thisdetail.itemsize.item.type}</h6>
                                                        <h6 className='cartInfo' >Size : {thisdetail.itemsize.size}</h6>
                                                        <h6 className='cartInfo'>
                                                            quantity : {thisdetail.quantity}
                                                        </h6>

                                                        <h6 className='cartInfo' >Delivery Status : {thisdetail.delId.deliveryStatus}</h6>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        )}

                    </div>
                </div>
                <br />
            </div>
        </div>
    )
}

// cartId: 30
// cartstatus: 0
// delId: {deliveryId: 20, payments: {…}, users: {…}, address: {…}, deliveryStatus: 'Order Received', …}
// hibernateLazyInitializer: {}
// itemsize:
// item:
// description: "pizza, dish of Italian origin consisting of a flattened disk of bread dough"
// itemName: "Momo Mia Pizza"
// itemid: 4
// type: "Veg"

// price: 250
// size: "Regular"
// sizeId: 8

// price: 250
// quantity: 1
// toppings: null




