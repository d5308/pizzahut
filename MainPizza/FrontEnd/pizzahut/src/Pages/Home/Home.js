import { motion, AnimatePresence } from 'framer-motion'
import React, { useState } from 'react'
import { useNavigate } from 'react-router'
import logo from '../../images/colorLogo.png'
import deal1 from '../../images/deal1.png'
import deal2 from '../../images/deal2.png'
import SignInModal from '../Modal/SignInModal/SignInModal'
import SignUpModal from '../Modal/SignUpModal/SignUpModal'
import './Home.css'

export default function Home() {

  const [UpModal, setsignUpModal] = useState(false)
  const openSignUp = () => setsignUpModal(true)
  const closeSignUp = () => setsignUpModal(false)

  const [InModal, setsignInModal] = useState(false)
  const openSignIn = () => setsignInModal(true)
  const closeSignIn = () => setsignInModal(false)

  const navigate = useNavigate()

  return (
    <div style={{ overflowX: "auto" }}>
      <div className='flatter'>
        <div className="row HeaderRow">
          <div className="col">
            <img src={logo} alt="Logo" className='logo' />
          </div>
          <div className="col">
            <motion.button className='float-end SignButton'
              whileHover={{ scale: 1.03 }}
              whileTap={{ scale: 0.98 }}
              onClick={() => (InModal ? closeSignIn() : openSignIn())}
            >Sign In</motion.button>
            {InModal && <SignInModal SignInModal={InModal} handclose={closeSignIn} />}

            <motion.button className='float-end SignButton'
              whileHover={{ scale: 1.03 }}
              whileTap={{ scale: 0.98 }}
              onClick={() => (UpModal ? closeSignUp() : openSignUp())}
            >Signup</motion.button>
            {UpModal && <SignUpModal signUpModal={UpModal} handclose={closeSignUp} />}

            <motion.button className='float-end btn btn-link linkBtn'
              whileHover={{ scale: 1.03, color: "burlywood" }}
              whileTap={{ scale: 0.98, color: "burlywood" }}
              onClick={() => (navigate('/pizza'))}
            >Menu</motion.button>

            <motion.button className='float-end btn btn-link linkBtn'
              whileHover={{ scale: 1.03, color: "burlywood" }}
              whileTap={{ scale: 0.98, color: "burlywood" }}
              onClick={() => (navigate('/aboutUs'))}
            >About</motion.button>
          </div>
        </div>
        <div>
          <div className="row">
            <div className="col">
              <div style={{ height: "150px" }}></div>
              <div className='' >
                <h1 className='text1'>Everytime you are hungry </h1>
                <h1 className='text1'>have Pizza's</h1><br />
                <motion.button className='orderButton'
                  whileHover={{ scale: 1.03, backgroundColor: "yellow" }}
                  onClick={() => (navigate('/pizza'))}
                >Order Online Now🍕</motion.button>
              </div>
              <div style={{ height: "150px" }}></div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <div className='container'>
        <div className="row">
          <div className="col">
            <motion.img src={deal1} alt=""
              onClick={() => (InModal ? closeSignIn() : openSignIn())}
              whileHover={{ cursor: "pointer" }}
            />
          </div>
          <div className="col">
            <motion.img src={deal2} alt=""
              onClick={() => (InModal ? closeSignIn() : openSignIn())}
              whileHover={{ cursor: "pointer" }}
            />
          </div>
        </div>
      </div><br />
      <br />
      <br />
      <br />
    </div>
  )
}
