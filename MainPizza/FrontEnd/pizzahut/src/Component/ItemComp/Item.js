import { useNavigate } from 'react-router'
import './Item.css'

export default function Item(props) {
    const { itemComp } = props
    // let id = itemComp.itemid
    const navigate = useNavigate()

    let string = `http://localhost:8080/itemImage/item/${itemComp.itemid}`
    return (
        <div>
            <div className="itembox shadow">
                <div class="card">
                    <img src={string} class="card-img-top img" alt="Item Image" />
                    <div class="card-body">
                        <h5 class="card-title">{itemComp.itemName}</h5>
                        <p class="card-text">{itemComp.type}</p>
                        <p class="card-text">{itemComp.description}...</p>
                        <a class="btn btn-primary"
                            onClick={() => {
                                navigate('/itemSize', { state: { itemid: itemComp.itemid } })
                            }}
                        >View Details</a>
                    </div>
                </div>
            </div>
            <br />
        </div>
    )
}