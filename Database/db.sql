DROP TABLE users, categories, pizza, pizza_size, cart, orders, orderDetails, payments;

-- !. Query for user table 
CREATE TABLE users(
    userid int primary key auto_increment,
    firstname varchar(20),
    lastname varchar(20),
    email varchar(40),
    password varchar(20),
    role varchar(20),
    -- address attributes
    plotNo varchar(20),
    streetName varchar(20),
    city varchar(20),
    state varchar(20),
    pincode varchar(6)
);

-- 2. Query for categories table
create TABLE categories(
    categoryId int primary key auto_increment,
    type varchar(10), -- veg/non-veg
    category varchar(20)  -- delux sides mangeritha...
);

--3. Query for pizza table
CREATE TABLE pizza(
    pizzaId int primary key auto_increment,
    categoryId int,
    pizzaName varchar(20),
    description varchar(100),
    foreign key(categoryId) references categories(categoryId)
);

--4. Query for Pizza_Size  (From where user will go to orders)
CREATE TABLE pizza_size(
    sizeId int primary key auto_increment,
    pizzaId int,
    size varchar(10),
    price double,
    foreign key(pizzaId) references pizza(pizzaId)
);

--5. Query for cart table   
CREATE TABLE cart(
    cartId int primary key auto_increment,
    userid int,
    sizeId int,
    quantity int,
    cartTime timestamp,
    foreign key(userid) references users (userid),
    foreign key(sizeId) references pizza_size (sizeId)
);

--6. Query for orders table
CREATE TABLE orders(
    orderId int primary key auto_increment,
    orderTime timestamp,
    userId int,
    cartId int,
    totalAmount double,
    mode varchar(10),
    status varchar(15),
    foreign key(userid) references users (userid),
    foreign key(cartId) references cart (cartId)
);  

--7. Query for payments table
CREATE TABLE payments(
    payId int primary key auto_increment,
    orderId int,
    totalAmount double, 
    pay_status varchar(10),
    orderTime timestamp,
    foreign key(orderId) references orders (orderId)
);

--8 Query for feedback Table
CREATE TABLE feedback(
    feedbackId int primary key auto_increment,
    userId int,
    feedback varchar(100),
    feedBackTime timestamp,
    foreign key(userid) references users (userid)
);