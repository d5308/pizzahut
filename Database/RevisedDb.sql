DROP TABLE  categories, pizza, pizza_size, cart, orders, orderhistory, payments,feedback,users;

-- !. Query for user table 
CREATE TABLE users(
    userId int primary key auto_increment,
    firstname varchar(20),
    lastname varchar(20),
    email varchar(40),
    phoneNo varchar(10),
    password varchar(20),
    role varchar(20)
   
);

-- One user can have many address
CREATE TABLE address(
    AddressId int primary key auto_increment,
    userId int,
    plotNo varchar(20),
    streetName varchar(20),
    city varchar(20),
    state varchar(20),
    pincode varchar(6),
    foreign key (userId) references users(userid)
);

-- 3. Query for categories table
create TABLE categories(
    categoryId int primary key auto_increment,
    type varchar(10), -- veg/non-veg
    category varchar(20)  -- delux sides mangeritha...
);

--4. Query for pizza table
CREATE TABLE pizza(
    pizzaId int primary key auto_increment,
    categoryId int,
    pizzaName varchar(20),
    description varchar(100),
    foreign key(categoryId) references categories(categoryId)
);

--5. Query for toppings table
CREATE TABLE toppings(
    toppingId int primary key auto_increment,
    pizzaId int,
    topping varchar(10),
    price double,
    foreign key(pizzaId) references pizza(pizzaId)
);


--6. Query for Pizza_Size  (From where user will go to orders)
CREATE TABLE pizza_size(
    sizeId int primary key auto_increment,
    pizzaId int,
    size varchar(10),
    price double,
    foreign key(pizzaId) references pizza(pizzaId)
);

CREATE TABLE beverages(
beverageId int primary key auto_increment,
beverageName varchar(20),
volume varchar(10),
price varchar(10)
);

--7. Query for cart table   
CREATE TABLE cart(
    cartId int primary key auto_increment,
    userid int,
    sizeId int,
    toppingId1 int,
    toppingId2 int,
    beverageId int,
    quantity int,
    price double,
    cartTime timestamp,
    foreign key(userid) references users (userid),
    foreign key(sizeId) references pizza_size (sizeId),
    foreign key(toppingId1) references toppings (toppingId),
    foreign key(toppingId2) references toppings (toppingId),
	foreign key(beverageId) references beverages (beverageId)
);

--8. Query for orders table
CREATE TABLE orders(
    orderId int primary key auto_increment,
    userId int,
    orderTime timestamp,
    totalAmount double,
    mode varchar(10),
    status varchar(15),
    foreign key(userid) references users (userid)
);  

--9. Query for payments table
CREATE TABLE payments(
    payId int primary key auto_increment,
    orderId int,
    totalAmount double, 
    pay_status varchar(10),
    orderTime timestamp,
    foreign key(orderId) references orders (orderId)
);

--10. Query for order_history table
CREATE TABLE order_history(
    orderHistoryID int primary key auto_increment,
    userid int,
    sizeId int,
    toppingId1 int,
    toppingId2 int,
    beverageId int,
    quantity int,
    price double,
    pay_status varchar(10),
    foreign key(userid) references users (userid),
    foreign key(sizeId) references pizza_size (sizeId),
    foreign key(toppingId1) references toppings (toppingId),
    foreign key(toppingId2) references toppings (toppingId),
	foreign key(beverageId) references beverages (beverageId)
);

--11. Query for delivery table
CREATE TABLE deliveryStatus(
    deliveryId int primary key auto_increment,
    orderId int,
    totalAmount double, 
    deliverystatus varchar(10),
    orderTime timestamp,
    foreign key(orderId) references orders (orderId)
);

--12 Query for feedback Table
CREATE TABLE feedback(
    feedbackId int primary key auto_increment,
    userId int,
    feedback varchar(100),
    feedBackTime timestamp,
    foreign key(userid) references users(userid)
);