        //**In user flow we required.**\\
        
1 => user sign up. insert/post in user
2 => user sign in. insert/post in user
3 => user delete account. delete/delete in user

4 => user add address. insert/post in address
5 => user add address. update/put in address

5 => user select on category. select/get in category
6 => user select on pizza. select/get in pizza

7 => user select size. select/get in pizza_size
8 => user select toppings. select/get in toppings

9 => user add pizza to cart. insert/post in cart
10 => user delete pizza to cart. delete/delete in cart

11 => user orders the pizza. insert into orders table

12 => user makes payment. insert into payment table

13 => after payment. add data in orderHistory. insert/post in orderDetails

14 =>user fills feedback . insert/post in feedback table


        //**In admin flow we required.**\\

1 => admin add Beverages. insert/post in Beverages
2 => admin update Beveragesy. update/put in Beverages
3 => admin delete Beverages. delete/delete in Beverages
4 => admin list of Beverages. select/get in Beverages
5=> admin list of Beverages. duplicate/get in Beverages

5 => admin add category. insert/post in categories
6 => admin update category. update/put in categories
7 => admin list of categories. select/get in categories
8 => admin delete category. delete/delete in categories

9 => admin add pizza. insert/post in pizza
10 => admin update pizza. update/put in pizza
11 => admin list of pizza. select/get in pizza
12 => admin delete pizza. delete/delete in pizza

13 => admin add pizza_size. insert/post in pizza_size
14 => admin update pizza_size. update/put in pizza_size
15 => admin list of pizza_size. select/get in pizza_size
16 => admin delete pizza_size. delete/delete in pizza_size

17 => admin add toppings. insert/post in toppings
18 => admin update toppings. update/put in toppings
19 => admin delete toppings. delete/delete in toppings
20 => admin list of toppings. select/get in toppings


21 => admin add delstatus. insert/post in delivery
22 => admin update delstatus. update/put in delivery

total 14 + 22 = 36
