CREATE TABLE orders(
    orderId int primary key auto_increment,
    userId int,
    orderTime timestamp,
    totalAmount double,
    mode varchar(10),
    status varchar(15),
    foreign key(userid) references user (userid)
);