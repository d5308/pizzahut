CREATE TABLE toppings(
    toppingId int primary key auto_increment,
    pizzaId int,
    topping varchar(10),
    price double,
    foreign key(pizzaId) references pizza(pizzaId)
)