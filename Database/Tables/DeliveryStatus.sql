CREATE TABLE DeliveryStatus(
    delId int primary key auto_increment,
    orderId int,
    totalAmount double, 
    order_status varchar(10),
    orderTime timestamp,
    foreign key(orderId) references orders (orderId)
);