CREATE TABLE User(
    userId int primary key auto_increment,
    firstname varchar(20),
    lastname varchar(20),
    email varchar(40),
    password varchar(20),
    role varchar(20)
);