CREATE TABLE cart(
    cartId int primary key auto_increment,
    userid int,
    sizeId int,
    toppingId1 int,
    toppingId2 int,
    quantity int,
    price double,
    cartTime timestamp,
    foreign key(userid) references user (userid),
    foreign key(sizeId) references pizza_size (sizeId),
    foreign key(toppingId1) references toppings (toppingId),
    foreign key(toppingId2) references toppings (toppingId)
);