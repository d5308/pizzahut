CREATE TABLE pizza(
    pizzaId int primary key auto_increment,
    categoryId int,
    pizzaName varchar(20),
    description varchar(100),
    foreign key(categoryId) references categories(categoryId)
);

insert into Pizza values
(default,1,'Momo Mia-Pizza','Epic combination of signature pan crust delicious paneer & veg momos topped with spicy Schezwan sauce onion capsicum sweet corn & 100% mozzarella'),
(default,2,'Momo Mia-Pizza','Epic combination of signature pan crust delicious paneer & veg momos topped with spicy Schezwan sauce onion capsicum sweet corn & 100% mozzarella'),
(default,3,'Margherita-Classic','Cheese & sauce'),
(default,4,'Veggie Supreme','black olives, green capsicum, mushroom, onion, red peprika, sweet corn'),
(default,5,'Chicken Supreme','herbed chicken, schezwan chicken, meatball, chicken tikka');