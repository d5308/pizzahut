CREATE TABLE feedback(
    feedbackId int primary key auto_increment,
    userId int,
    feedback varchar(100),
    feedBackTime timestamp,
    foreign key(userid) references user(userid)
);