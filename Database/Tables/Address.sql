CREATE TABLE Address(
    AddressId int primary key auto_increment,
    userId int,
    plotNo varchar(20),
    streetName varchar(20),
    city varchar(20),
    state varchar(20),
    pincode varchar(6),
    foreign key (userId) references user(userid)
);