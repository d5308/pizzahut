create TABLE categories(
    categoryId int primary key auto_increment,
    type varchar(10), -- veg/non-veg
    category varchar(20)  -- delux sides mangeritha...
);

insert into Categories values
(default,'Veg','Momo-mia'),
(default,'Non-Veg','Momo-mia'),
(default,'Veg','Classic'),
(default,'Veg','Supreme'),
(default,'Non-Veg','Supreme');