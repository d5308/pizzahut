CREATE TABLE pizza_size(
    sizeId int primary key auto_increment,
    pizzaId int,
    size varchar(10),
    price double,
    foreign key(pizzaId) references pizza(pizzaId)
);

insert into pizza_size values
(default,1,'Regular','269'),
(default,1,'Medium','449');

insert into pizza_size values
(default,'2','Regular','299'),
(default,'2','Medium','499');

insert into pizza_size values
(default,'3','Regular','129'),
(default,'3','Medium','279');

insert into pizza_size values
(default,'4','Regular','329'),
(default,'4','Medium','569');

insert into pizza_size values
(default,'5','Regular','359'),
(default,'5','Medium','629');