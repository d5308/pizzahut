CREATE TABLE payments(
    payId int primary key auto_increment,
    orderId int,
    totalAmount double, 
    pay_status varchar(10),
    orderTime timestamp,
    foreign key(orderId) references orders (orderId)
);